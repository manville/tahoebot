# -*- coding: utf-8 -*-
"""Interactor Class."""
from __future__ import annotations

import abc
import inspect
import typing as t
from dataclasses import dataclass

from tahoebot.exceptions import InvalidRequestError
from tahoebot.utils.collections import Bunch, frozendict
from tahoebot.utils.dependency_injection import Container
from tahoebot.utils.functools import reify


Args = t.Sequence[t.Any]
Kwargs = t.Mapping[str, t.Any]

RequestModel = Bunch


class ResponseModel(Bunch):
    """Response Model."""
    errors: Kwargs
    data: Kwargs

    @reify
    def is_success(self):
        """Check for errors in the resopnse."""
        return not self.errors


class RequestSchema(Bunch):
    """Schema for validating request arguments."""

    def construct(self, input_data: Kwargs) -> RequestModel:
        """Validate input_data."""
        if not all(key in input_data for key, val in self.items()
                   if 'default' not in val):
            raise InvalidRequestError(self)
        data = {key: val['default'] for key, val in self.items()
                if 'default' in val}
        try:
            data.update({key: self[key]['type'](value)
                        for key, value in input_data.items()})
        except ValueError:
            raise InvalidRequestError(self)

        return RequestModel(**data)

    __call__ = construct

    @classmethod
    def from_execute(cls,
                     execute: t.Callable[..., ResponseModel]
                     ) -> RequestSchema:
        """Create a schema from execute signature."""
        parameters = inspect.signature(execute).parameters.values()
        fields = {}
        for parameter in parameters:
            if parameter.name == 'self':
                continue
            field = {'type': parameter.annotation}
            if parameter.default is not parameter.empty:
                field['default'] = parameter.default
            fields[parameter.name] = field
        return cls(**fields)


class Interactor:
    """Application specific operation or query.

    This is the core class of the application layer. It represents
    an application-specific operation that can be taken or a query to ask.

    Attributes:
        container (Container): Contains dependencies for this interactor.
    """
    container: Container

    def __init__(self, container):
        """Init Interactor."""
        self.container = container

    @reify
    def schema(self) -> RequestSchema:
        """Extract the schema for this interactor."""
        return RequestSchema.from_execute(self.execute)

    def invoke(self, **input_data: Kwargs) -> ResponseModel:
        """Execute the operation defined by the use_case."""
        try:
            request = self.schema(input_data)
            result = self.execute(**request)
        except Exception as e:
            response = ResponseModel(errors=str(e))
        else:
            response = ResponseModel(data=result)
        return response

    __call__ = invoke

    @abc.abstractmethod
    def execute(self, *args: Args, **kwargs: Kwargs) -> ResponseModel:
        """Execute interactor actions.

        A hook for the key part of the interactor process.
        """
        raise NotImplementedError
